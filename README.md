# Wooden-Puzzle

Old project that involved solving a specific Klotski wooden puzzle game. Solving routine used breadth-first search. Then blocks are animated to move based on which moves to perform.

![alt text](https://dl.dropboxusercontent.com/u/2920751/solve1.png)
![alt text](https://dl.dropboxusercontent.com/u/2920751/solve2.png)

<h2>Solver</h2>

Solving the Klotski puzzle is done using breadth-first search with Huffman encoding done on the board states to save time from looking at duplicate states in other parts of the tree.
